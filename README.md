# React + Vite

## Fetch South Park API

* Find API documentation [here](https://spapi.dev/docs#baseUrl)

```js
// Base URL for the South Park API is
const base_api = 'https://spapi.dev/api'

// Searching (like characters name as eric)
fetch(`${base_api}/characters?search=${name}`)

// get all the character resources 
fetch(`${base_api}/characters`)

// get a specific character resource
fetch(`${base_api}/characters/${id}`)

// Episodes Endpoints of the South Park series
// get all the episode resources
fetch(`${base_api}/episodes`)

// get a specific episode resource
fetch(`${base_api}/episodes/${id}`)
```

## Code

```jsx```
```jsx```
```jsx```
```jsx```
```jsx```
