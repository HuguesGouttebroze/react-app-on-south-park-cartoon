/* eslint-disable no-unused-vars */
import { useState } from 'react'
import { Routes, Route, Outlet, Link } from 'react-router-dom'
import './App.css'
import Characters from './components/Characters'
import Layout from './components/Layout'
import Episodes from './components/Episodes'
import Families from './components/Families'
import Locations from './components/Locations'
import EpisodeDetail from './components/EpisodeDetail'
import CharacterDetail from './components/CharacterDetail'

import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function Home() {
  return (
    <div>
      <h2>Welcome to South park home</h2>
    </div>
  );
}

function NoMatch() {
  return (
    <div>
      <h2>Nothing to see here! So Get Out!</h2>
      <p>
        <Link to="/">Go to the home page</Link>
      </p>
    </div>
  );
}

function App() {
  return (
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        <h1>Dumb on South park</h1>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="characters" element={<Characters />} />
            <Route path="characters/:id" element={<CharacterDetail />} />
            <Route path="episodes" element={<Episodes />} /> 
            <Route path="episodes/:id" element={<EpisodeDetail />} />              
            <Route path="families" element={<Families />} />
            <Route path="locations" element={<Locations />} />
            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </ThemeProvider>
  )
}

export default App
