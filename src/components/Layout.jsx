/* eslint-disable no-unused-vars */
import {Link, Outlet} from 'react-router-dom'
import Box from '@mui/material/Box';
import sp1 from '../assets/sp1.jpg'
import sp2 from '../assets/sp2.jpg'
import sp3 from '../assets/sp3.jpg'
import sp4 from '../assets/sp4.jpg'
import Carousel from './Carousel';

export default function Layout() {
  const imgList = [
      {name: sp1}, 
      {name: sp2}, 
      {name: sp3}, 
      {name: sp4}
  ]

  return (
    <div>
      <header>
        {/* <img src={sp2} alt="halloween night" width={512} /> */}    
        <Carousel width={512} /> 
        <nav>
          <ul>
            <li>
              <Link to="/">South Park home</Link>
            </li>
            <li>
              <Link to="/characters">All characters</Link>
            </li>
            <li>
              <Link to="/episodes">Episodes</Link>
            </li>
            <li>
              <Link to="/families">Families</Link>
            </li>
            <li>
              <Link to="/locations">Locations</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>          
            <li>
              <Link to="/member">Member</Link>
            </li>
          </ul>
        </nav>
        <hr />
        <Outlet />
        {
          imgList.map((item) => {
            return (
              <img 
                key={item} 
                src={item.name} 
                width={256}
              />
            )
          })
        }
      </header>
    </div>
  );
}
