/* eslint-disable no-unused-vars */
import React from 'react'
import CircularProgress from '@mui/material/CircularProgress'
import { useFetch } from '../hooks/useFetch'
import {Link} from 'react-router-dom'

export default function Characters() {
  const {data, isDataLoading} = useFetch('https://spapi.dev/api/characters')
  const fetchData = data?.data
  console.log(fetchData)

  return isDataLoading ? (
    <CircularProgress />
  ) : (
    <div>
      <h3>Here is all South Park characters</h3>
      {
        fetchData && fetchData.map((data) => {
          return (
            <div key={data.id}>
              <Link to={`/episodes/${data.id}`}>
                {data.name}
              </Link>
            </div>
          )
        })
      }
    </div>
  )
}
