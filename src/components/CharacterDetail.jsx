/* eslint-disable no-unused-vars */
import React from 'react'
import { useParams, Link } from 'react-router-dom'
import { useFetch } from '../hooks/useFetch'
import CircularProgress from '@mui/material/CircularProgress'

export default function CharacterDetail() {
const { id } = useParams()
  const {data, isDataLoading} = useFetch(`https://spapi.dev/api/characters/${id}`)
  const fetchOneCharacter = data?.data
  console.log(fetchOneCharacter) 

  return isDataLoading ? ( 
    <CircularProgress /> 
      ) : (
    <div>
      <h3>{ id }</h3>
      <h2>{fetchOneCharacter?.name}</h2>
      <Link to='/characters'>Back to characters list</Link>
    </div>
  )
}
