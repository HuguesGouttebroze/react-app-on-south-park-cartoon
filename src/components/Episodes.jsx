/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import { useFetch } from '../hooks/useFetch'
import { Link, useNavigate } from 'react-router-dom'
// MUI tools import
import CircularProgress from '@mui/material/CircularProgress'
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Button } from '@mui/material';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function Episodes() {
  const [pageNumber, setPageNumber] = useState(1)
  const {data, isDataLoading} = useFetch(`https://spapi.dev/api/episodes?page=${pageNumber}`)
  const fetchEpisodesData = data?.data
  console.log(fetchEpisodesData)

  const handleNextPage = () => {
    setPageNumber((prev) => prev + 1)
    //navigate(`episodes`) 
  }
  const handlePrevPage = () => {
    setPageNumber((prev) => prev - 1)
  }
  
  return isDataLoading ? (
    <CircularProgress />
      ) : (
        <div className='wrapper'>
          <h3>Here is all South Park episodes</h3>
          <Grid>
            <div className="details">
            {
              fetchEpisodesData && fetchEpisodesData.map((data) => {
                return (
                  <div className='details' key={data.id}>              
                    <Grid>
                      <Item>
                        <img 
                          src={data.thumbnail_url.slice(0, -34)} 
                          alt={data.name} 
                          width={250}  
                        />
                        <h5>{data.name}</h5>
                        <p>{data.description}</p>
                        <Link to={`/episodes/${data.id}`}>More details</Link>
                      </Item>
                    </Grid>
                  </div>
                )
              })
            }
          </div>
        </Grid>
        {
          pageNumber > 1 && (
            <Button 
              color='warning' 
              variant="contained"
              onClick={handlePrevPage}
            >
              Previous Episodes
            </Button>
          )
        }       
        <Button 
          color='success' 
          variant="contained"
          onClick={handleNextPage}
        >
          Next Episodes
        </Button>
      </div>
    )
}
