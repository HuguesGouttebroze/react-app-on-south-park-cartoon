/* eslint-disable no-unused-vars */
import React from 'react' 
import { useParams, Link } from 'react-router-dom'
import { useFetch } from '../hooks/useFetch'
import CircularProgress from '@mui/material/CircularProgress'

export default function EpisodeDetail() {
  const { id } = useParams()
  const {data, isDataLoading} = useFetch(`https://spapi.dev/api/episodes/${id}`)
  const ep = data?.data
  console.log(ep) 
  
  const imgSrc = ep?.thumbnail_url
  const imgSlice = imgSrc?.slice(0, -34)
  console.log(imgSlice);
  
  return isDataLoading ? ( 
    <CircularProgress /> 
      ) : (
    <div>
      <h3>Episode { id }</h3>
      <h2>{ep?.name}</h2>
      <p>{ep?.description}</p>
      <p>{ep?.season}</p>
      <img 
        src={imgSlice} 
        width={420}
      />
      <Link to='/episodes'>See all episodes</Link>
    </div>
  )
}
