/* eslint-disable react/prop-types */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-unused-vars */
import { Button } from '@mui/material'
import React, {useState} from 'react'
import sp1 from '../assets/sp1.jpg'
import sp2 from '../assets/sp2.jpg'
import sp3 from '../assets/sp3.jpg'
import sp4 from '../assets/sp4.jpg'

const imgUrlList = [
  {
    name: '',
    ep: 'Season 16, Episode 12',
    url: 'https://www.denofgeek.com/wp-content/uploads/2021/10/South-Park-Avengers-Halloween.jpg',
    alt: '',
  },
  {
    name: 'SOUTH PARK, The INCREDIBLE...',
    about: 'The boys go trick-or-treating as The Avengers',
    ep: 'Season 16, Episode 12',
    date: '• 10/24/2012',
    url: 'https://images.paramount.tech/uri/mgid:arc:imageassetref:shared.southpark.us.en:18b1fe48-fd89-41eb-b5e7-81c3df7aa864?quality=0.7&gen=ntrn&legacyStatusCode=true',
    alt: 'SOUTH PARK, The INCREDIBLE...',
  },
  {
    name: '..........',
    ep: 'Season ..., Episode ...',
    url: 'https://m.media-amazon.com/images/M/MV5BOGU0M2VmZTctOWM0YS00NDI5LWEzZjUtZDZkMGYzYzU0NmJjXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_.jpg',
    alt: '',
  },
  {
    name: '............',
    ep: 'Season ..., Episode ...',
    url: 'https://images.paramount.tech/uri/mgid:arc:imageassetref:shared.southpark.global:60b198fd-e14c-48f4-8c96-0985c69235c7?quality=0.7&gen=ntrn&legacyStatusCode=true',
    alt: '',
  },
  {
    name: '...........',
    ep: 'Season ..., Episode ..',
    url: 'https://play-lh.googleusercontent.com/3_TY71vaVOGfhKs9cbPa0yx0NrJU66DCKkMrcYd48L8xJ9UUCpgGBD47GXS50LtVlh8X',
    alt: '',
  },
  {
    name: '...........',
    ep: 'Season ......., Episode .....',
    url: 'https://images.paramount.tech/uri/mgid:arc:imageassetref:shared.southpark.global:c191b7d1-bf73-4dce-82e3-9ecf040a13f6?quality=0.7&gen=ntrn',
    alt: '',
  },
  {
    name: '........',
    ep: 'Season 16, Episode 12',
    url: sp1,
    alt: '',
  },{
    name: '',
    ep: 'Season 16, Episode 12',
    url: sp2,
    alt: '',
  },{
    name: '',
    ep: 'Season 16, Episode 12',
    url: sp3,
    alt: '',
  },{
    name: '',
    ep: 'Season 16, Episode 12',
    url: sp4,
    alt: '',
  },
  {
    name: '',
    ep: 'Season 16, Episode 12',
    url: 'https://i.ytimg.com/vi/iNE5768rqGM/hqdefault.jpg',
    alt: '',
  },
  {
    name: 'I Can\'t Find Turner and Hooch, A Nightmare on Face Time',
    url: 'https://southpark-online.nl/assets/images/clips/i-cant-find-turner-and-hooch.jpg',
    alt: '',
  },
  {
    name: '###@#&@"%**#',
    url: 'https://southpark-online.nl/assets/images/clips/the-blockbustering.jpg',
    alt: '',
  },
  {
    name: '',
    ep: 'Season 16, Episode 12',
    url: 'https://24.media.tumblr.com/tumblr_mcj1ef5hbL1qasvg8o1_500.png',
    alt: '',
  },
  {
    name: 'Randy as Shining',
    ep: 'Season 16, Episode 12',
    url: 'https://7minutemiles.com/wp-content/uploads/2012/10/RandyShining.jpg',
    alt: 'Randy as Shining',
  }
]
export default function Carousel({width}) {
  const [index, setIndex] = useState(0)
  const [showMore, setShowMore] = useState(false)
  const hasNext = index < imgUrlList.length - 1

  function handleNextClick() {
    if (hasNext) {
      setIndex(index + 1) // update array img state
    } else {
      setIndex(0) // else if they is no more img, go back to the first img (with index 0)
    }
  }

  function handleMoreClick() {
    setShowMore(!showMore) // update if true
  }

  let drawImage = imgUrlList[index]

  return (
    <>
      <img 
        src={drawImage.url} 
        alt={drawImage.name} 
        width={width}
      />
      <h3>
        ({index + 1} on {imgUrlList.length})
      </h3><Button onClick={handleNextClick}>
        Next Picture
      </Button>
      <Button onClick={handleMoreClick}>
        {showMore ? 'Hide' : 'Show'} Show more
      </Button>
      <h2>
        <i>{drawImage.name}</i> in {drawImage.ep}
      </h2>
      {showMore && <p>{drawImage.about}</p>}
      
    </>
  )
}
